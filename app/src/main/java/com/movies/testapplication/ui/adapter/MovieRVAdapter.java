package com.movies.testapplication.ui.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.movies.testapplication.R;
import com.movies.testapplication.model.movie.Movie;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import dagger.internal.Preconditions;


public class MovieRVAdapter extends RecyclerView.Adapter<MovieRVAdapter.MoviesViewHolder> implements Filterable {

    private ArrayList<Movie> moviesDataSet = new ArrayList<>();

    private ArrayList<Movie> backupDataSet = new ArrayList<>();

    public MovieRVAdapter() {
    }

    @SuppressWarnings("unused")
    public MovieRVAdapter(ArrayList<Movie> moviesDataSet) {
        this.moviesDataSet = Preconditions.checkNotNull(moviesDataSet);
    }

    @SuppressWarnings("unused")
    public ArrayList<Movie> getMoviesDataSet() {
        return moviesDataSet;
    }

    @SuppressWarnings("unused")
    public void setMoviesDataSet(ArrayList<Movie> moviesDataSet) {
        this.moviesDataSet = moviesDataSet;
    }

    @SuppressWarnings("unused")
    public ArrayList<Movie> getBackupDataSet() {
        return backupDataSet;
    }

    @SuppressWarnings("unused")
    public void setBackupDataSet(ArrayList<Movie> backupDataSet) {
        this.backupDataSet = backupDataSet;
    }

    public void backupData() {
        backupDataSet.clear();
        backupDataSet.addAll(moviesDataSet);
    }

    public void restoreData() {
        if (backupDataSet.size() > 0) {
            moviesDataSet.clear();
            moviesDataSet.addAll(backupDataSet);
        }
    }

    public void expandDataSet(ArrayList<Movie> newDataSet) {
        Preconditions.checkNotNull(moviesDataSet).addAll(newDataSet);
        backupDataSet.clear();
        backupDataSet.addAll(moviesDataSet);
    }

    public void clearData() {
        moviesDataSet.clear();
    }

    @Override
    public Filter getFilter() {

        restartBuffer();

        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                ArrayList<Movie> filteredResults;
                if (constraint == null || constraint.length() == 0) {
                    filteredResults = moviesDataSet;
                } else {
                    filteredResults = getFilteredResults(constraint.toString().toLowerCase());
                }

                FilterResults results = new FilterResults();
                results.values = filteredResults;

                return results;
            }

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                moviesDataSet = (ArrayList<Movie>) results.values;
                notifyDataSetChanged();
            }
        };
    }

    private void restartBuffer() {
        if (moviesDataSet == null) {
            moviesDataSet = new ArrayList<>();
            moviesDataSet.addAll(backupDataSet);
        }
    }

    private ArrayList<Movie> getFilteredResults(String constraint) {
        ArrayList<Movie> results = new ArrayList<>();

        for (Movie data : moviesDataSet) {

            StringBuilder builder = new StringBuilder();

            for (String genre : data.getGenre()) {
                builder.append(genre).append(",");
            }

            if (String.valueOf(data.getRating()).toLowerCase().contains(constraint)
                    || String.valueOf(data.getReleaseYear()).toLowerCase().contains(constraint)
                    || builder.toString().toLowerCase().contains(constraint)) {
                results.add(data);
            }
        }
        return results;
    }

    @Override
    public MoviesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.movie_card, parent, false);

        return new MoviesViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MoviesViewHolder holder, int position) {

        restartBuffer();

        Movie movie = moviesDataSet.get(position);

        holder.tvTitle.setText(movie.getTitle() + " (" + String.valueOf(movie.getReleaseYear()) + ")");

        if (movie.getParsedGenres() != null && !movie.getParsedGenres().isEmpty()) {
            holder.tvGenres.setText(movie.getParsedGenres());
        } else {
            StringBuilder builder = new StringBuilder(" ");
            for (String genre : movie.getGenre()) {
                builder.append(genre).append(" ");
            }
            holder.tvGenres.setText(builder.toString());
        }

        Picasso.with(holder.ivThumbnail.getContext())
                .load(movie.getImage())
                .into(holder.ivThumbnail);

        holder.ratingBar.setRating(movie.getRating() / 2);
        holder.ratingBar.setIsIndicator(true);

    }

    @SuppressWarnings("WeakerAccess")
    public static class MoviesViewHolder extends RecyclerView.ViewHolder {

        TextView tvTitle;

        @SuppressWarnings("unused")
        TextView tvYear;

        TextView tvGenres;

        ImageView ivThumbnail;

        RatingBar ratingBar;

        public MoviesViewHolder(View itemView) {
            super(itemView);

            this.tvTitle = (TextView) itemView.findViewById(R.id.tv_title_text);
            this.ratingBar = (RatingBar) itemView.findViewById(R.id.ratingBar);
            /*this.tvYear = (TextView) itemView.findViewById(R.id.tv_year_text);*/
            this.tvGenres = (TextView) itemView.findViewById(R.id.tv_genres_text);
            this.ivThumbnail = (ImageView) itemView.findViewById(R.id.iv_thumbnail);
        }
    }

    @Override
    public int getItemCount() {
        restartBuffer();
        return moviesDataSet.size() ;
    }

}
