package com.movies.testapplication.ui.activity;


import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;

import com.movies.testapplication.R;


public abstract class BaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getContentView() > 0)
            setContentView(getContentView());

        commonStyling();

    }

    @SuppressWarnings({"ConstantConditions", "deprecation"})
    public void commonStyling() {

        ActionBar actionBar = getSupportActionBar();

        actionBar.setDisplayOptions(/*ActionBar.DISPLAY_SHOW_HOME |*/
                ActionBar.DISPLAY_SHOW_TITLE /*| ActionBar.DISPLAY_HOME_AS_UP*/);

        actionBar.setTitle(getResources().getString(R.string.app_name));
        actionBar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorSecondPrimary)));
        //actionBar.setDisplayHomeAsUpEnabled(true);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    protected abstract int getContentView();

}
