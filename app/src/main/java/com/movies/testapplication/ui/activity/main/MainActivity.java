package com.movies.testapplication.ui.activity.main;

import android.Manifest;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.movies.testapplication.R;
import com.movies.testapplication.presenters.movies.MoviesActivityPresenter;
import com.movies.testapplication.listeners.movie.IMovieDataInteractorUser;
import com.movies.testapplication.listeners.movie.IMovieDataProviderInteractor;
import com.movies.testapplication.model.movie.Movie;
import com.movies.testapplication.ui.activity.BaseActivity;
import com.movies.testapplication.ui.fragment.main.MovieFragment;

import java.util.List;

import javax.inject.Inject;

public class MainActivity extends BaseActivity implements IMovieDataProviderInteractor {

    private static final String TAG = MainActivity.class.getSimpleName();

    private static final int INTERNET_PERMISSION_REQUEST = 999;

    private MovieFragment movieFragment;

    private MoviesActivityPresenter contactInteractor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);

        contactInteractor = new MoviesActivityPresenter();

        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.INTERNET)
                == PackageManager.PERMISSION_GRANTED) {

            setupTabLayoutComponents();
            setCurrentFragment(0);
        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.INTERNET},
                    INTERNET_PERMISSION_REQUEST);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case INTERNET_PERMISSION_REQUEST: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    setupTabLayoutComponents();
                    setCurrentFragment(0);

                } else {
                    Log.d(TAG, "Permission request sent");
                }
            }
        }
    }

    /**
     * Initialize child components
     */
    private void setupTabLayoutComponents() {

        //initialize  fragments instances
        movieFragment = new MovieFragment();
    }

    /**
     * Replace old fragment with new one
     */
    public void setCurrentFragment(int tabPosition) {
        switch (tabPosition) {
            case 0:
                replaceFragment(movieFragment);
                break;
            default:
                replaceFragment(movieFragment);
                break;
        }
    }

    /**
     * Replace fragment procedure
     *
     * @param fragment new chosed procedure
     */
    public void replaceFragment(Fragment fragment) {

        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.mainViewHolder, fragment);
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        ft.commit();

    }

    @Override
    protected int getContentView() {
        return R.layout.activity_main;
    }

    @Override
    public void stopInteraction() {
        contactInteractor.stopInteraction();
    }

    @Override
    public List<Movie> entitiesProviding(List<Movie> movieList, IMovieDataInteractorUser interactor) {
        contactInteractor.getAllMovies(interactor);
        return null;
    }

    @Override
    public List<Movie> entitiesObtaining(List<Movie> movies, IMovieDataInteractorUser iMovieDataInteractorUser) {
        return null;
    }

    @Override
    public void clearData() {
        contactInteractor.clearData();
    }
}
