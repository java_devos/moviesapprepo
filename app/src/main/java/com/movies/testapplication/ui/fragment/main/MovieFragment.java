package com.movies.testapplication.ui.fragment.main;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.movies.testapplication.R;
import com.movies.testapplication.listeners.movie.IMovieDataInteractorUser;
import com.movies.testapplication.model.movie.Movie;
import com.movies.testapplication.ui.adapter.MovieRVAdapter;
import com.movies.testapplication.ui.fragment.BaseFragment;
import com.movies.testapplication.listeners.movie.IMovieDataProviderInteractor;

import java.util.ArrayList;
import java.util.List;

public class MovieFragment extends BaseFragment implements IMovieDataInteractorUser, SearchView.OnQueryTextListener {

    @SuppressWarnings("unused")
    private static final String TAG = MovieFragment.class.getSimpleName();

    private SwipeRefreshLayout swipeRefreshLayout;

    @SuppressWarnings("FieldCanBeLocal")
    private RecyclerView recyclerView;

    private SearchView searchView;

    private LinearLayoutManager layoutManager;

    private MovieRVAdapter rvAdapter;

    public ArrayList<Movie> movies = new ArrayList<>();

    private IMovieDataProviderInteractor interactor;

    //Layout manager counters
    private int previousItemCounter, currentItemCounter, totalItemCounter;

    public MovieFragment() {
        // Required empty public constructor
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        interactor = (IMovieDataProviderInteractor)getActivity();
        setHasOptionsMenu(true);

        initViews(view);
        interactor.entitiesProviding(null, this);

    }

    @Override
    public void onResume() {
        super.onResume();
        interactor.entitiesProviding(null, this);
    }

    @Override
    public void onPause() {
        this.stopInteraction();
        super.onPause();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_options, menu);
        final MenuItem item = menu.findItem(R.id.action_search);
        searchView = (SearchView) MenuItemCompat.getActionView(item);
        MenuItemCompat.setOnActionExpandListener(item,
                new MenuItemCompat.OnActionExpandListener() {
                    @Override
                    public boolean onMenuItemActionExpand(MenuItem menuItem) {
                        rvAdapter.backupData();
                        recyclerView.removeOnScrollListener(scrollListener);
                        return true;
                    }

                    @Override
                    public boolean onMenuItemActionCollapse(MenuItem menuItem) {
                        rvAdapter.restoreData();
                        rvAdapter.notifyDataSetChanged();
                        recyclerView.addOnScrollListener(scrollListener);
                        return true;
                    }
                });

        ImageView closeButton = (ImageView) searchView.findViewById(R.id.search_close_btn);
        closeButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                searchView.setQuery("", false);
                rvAdapter.restoreData();
                rvAdapter.notifyDataSetChanged();
            }
        });

        searchView.setOnQueryTextListener(this);

    }

    @Override
    public boolean onQueryTextChange(String newText) {
        rvAdapter.getFilter().filter(newText);
        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    private void initViews(View view) {
        swipeRefreshLayout = (SwipeRefreshLayout)view.findViewById(R.id.swipeRefreshLayout);
        recyclerView = (RecyclerView) view.findViewById(R.id.movies_recycler_view);
        rvAdapter = new MovieRVAdapter();
        layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(rvAdapter);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // Reset items
                refreshItems();
            }
        });

        recyclerView.addOnScrollListener(scrollListener);

    }

    RecyclerView.OnScrollListener scrollListener =  new RecyclerView.OnScrollListener() {
        @Override
        public void onScrolled(final RecyclerView recyclerView, int dx, int dy) {
            if (dy > 0) {

                initCounters();

                if ((currentItemCounter + previousItemCounter) >= totalItemCounter) {

                        interactor.entitiesProviding(null, MovieFragment.this);

                }
            }
        }
    };

    void refreshItems() {
        this.clearData();
        rvAdapter.clearData();
        rvAdapter.notifyDataSetChanged();
        onItemsLoadComplete();
    }

    void onItemsLoadComplete() {

        interactor.entitiesProviding(null, this);

        // Stop refresh animation
        swipeRefreshLayout.setRefreshing(false);
    }

    /**
     * obtain layoutManager counters values
     */
    private void initCounters() {

        currentItemCounter = layoutManager.getChildCount();
        totalItemCounter = layoutManager.getItemCount();
        previousItemCounter = layoutManager.findFirstVisibleItemPosition();
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_movies_list;
    }

    @Override
    public List<Movie> entitiesProviding(List<Movie> movies, Void aVoid) {
        return null;
    }

    @Override
    public List<Movie> entitiesObtaining(List<Movie> movies, Void aVoid) {
        ArrayList<Movie> mvs = new ArrayList<>(movies);
        rvAdapter.expandDataSet(mvs);
        rvAdapter.notifyDataSetChanged();
        return movies;
    }

    @Override
    public void stopInteraction() {
        interactor.stopInteraction();
    }

    @Override
    public void clearData() {
        interactor.clearData();
    }
}
