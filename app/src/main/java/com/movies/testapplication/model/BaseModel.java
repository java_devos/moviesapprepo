package com.movies.testapplication.model;

import java.io.Serializable;
import java.util.UUID;


public class BaseModel extends Auditable implements Serializable {

    private static final String TAG = BaseModel.class.getSimpleName();

    private static final long GUID = UUID.randomUUID().getMostSignificantBits();

    private long id;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public static long getGUID() {
        return GUID;
    }
}
