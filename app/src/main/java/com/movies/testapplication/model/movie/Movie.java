package com.movies.testapplication.model.movie;

import com.movies.testapplication.model.BaseModel;

import java.util.List;


public class Movie extends BaseModel{

    private String title;

    private String image;

    private Float rating;

    private Integer releaseYear;

    private List<String> genre;

    private String parsedGenres;

    public Movie() {
    }

    public Movie(String title, String image, Float rating, Integer releaseYear, List<String> genre, String parsedGenres) {
        this.title = title;
        this.image = image;
        this.rating = rating;
        this.releaseYear = releaseYear;
        this.genre = genre;
        this.parsedGenres = parsedGenres;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Float getRating() {
        return rating;
    }

    public void setRating(Float rating) {
        this.rating = rating;
    }

    public Integer getReleaseYear() {
        return releaseYear;
    }

    public void setReleaseYear(Integer releaseYear) {
        this.releaseYear = releaseYear;
    }

    public List<String> getGenre() {
        return genre;
    }

    public void setGenre(List<String> genre) {
        this.genre = genre;
    }

    public String getParsedGenres() {
        StringBuilder builder;
        if (this.parsedGenres == null || this.parsedGenres.isEmpty()) {
            builder = new StringBuilder("");
            for (String genre : this.getGenre()) {
                builder.append(genre).append(" ");
            }
            return builder.toString();
        } else {
            return this.parsedGenres;
        }

    }

    public void setParsedGenres(String parsedGenres) {
        this.parsedGenres = parsedGenres;
    }

    @Override
    public String toString() {
        return "Movie{" +
                "title='" + title + '\'' +
                ", image='" + image + '\'' +
                ", rating=" + rating +
                ", releaseYear=" + releaseYear +
                ", genre=" + genre +
                ", parsedGenres='" + parsedGenres + '\'' +
                '}';
    }
}
