package com.movies.testapplication.tools;


import java.io.IOException;

public class CommoTools {

    public static boolean checkIsNetworkAvailable(){
        String command = "ping -c 1 google.com";
        try {
            return (Runtime.getRuntime().exec (command).waitFor() == 0);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (@SuppressWarnings("TryWithIdenticalCatches") IOException e) {
            e.printStackTrace();
        }

        return false;
    }
}
