package com.movies.testapplication.presenters.movies;


import android.util.Log;

import com.movies.testapplication.tools.CommoTools;
import com.movies.testapplication.api.clients.rest.RetrofitClient;
import com.movies.testapplication.api.gateways.rest.RestApi;
import com.movies.testapplication.application.App;
import com.movies.testapplication.database.dao.MovieDAO;
import com.movies.testapplication.presenters.BasePresenter;
import com.movies.testapplication.listeners.movie.IMovieDataInteractorUser;
import com.movies.testapplication.model.movie.Movie;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;


public class MoviesActivityPresenter extends BasePresenter<Movie, Void> {

    private static final String TAG = MoviesActivityPresenter.class.getSimpleName();

    @Inject
    RetrofitClient retrofitClient;

    @Inject
    MovieDAO movieDAO;

    private RestApi client;

    private Disposable disposable;

    private IMovieDataInteractorUser interactor;

    public MoviesActivityPresenter() {
        App.getMainComponent().inject(this);
        this.client = retrofitClient.getClientApi();
    }

    public void getAllMovies(final IMovieDataInteractorUser interactor) {

        this.interactor = interactor;
        this.obtainEntities();
    }

    @Override
    public void obtainEntities() {
        if (CommoTools.checkIsNetworkAvailable()) {
            disposable = client.getPhones()
                    .observeOn(AndroidSchedulers.mainThread()).
                            subscribeOn(Schedulers.io()).subscribe(
                            provideOnNextConsumer(interactor),
                            provideOnErrorConsumer(),
                            provideOnCompleteConsumer());
        } else {
            interactor.entitiesObtaining(movieDAO.obtainAll(), null);
        }
    }

    @Override
    public void persistEntities(final List<Movie> movies) {
        Thread myThread = new Thread(new Runnable() {

            @Override
            public void run() {
                for (Movie movie : movies) {
                    movieDAO.save(movie);
                }
            }
        });
        myThread.start();
    }

    @Override
    public void stopInteraction() {
        if (disposable != null && !disposable.isDisposed()) {
            disposable.dispose();
        }
    }

    private Consumer<List<Movie>> provideOnNextConsumer(final IMovieDataInteractorUser interactor) {
        return new Consumer<List<Movie>>() {
            @Override
            public void accept(@NonNull List<Movie> movieList) throws Exception {

                @SuppressWarnings("MismatchedQueryAndUpdateOfCollection") ArrayList<Movie> movies = new ArrayList<>();
                movies.addAll(movieList);

                interactor.entitiesObtaining(movieList, null);
                MoviesActivityPresenter.this.persistEntities(movieList);
            }
        };
    }

    private Consumer<Throwable> provideOnErrorConsumer() {
        return new Consumer<Throwable>() {
            @Override
            public void accept(@NonNull Throwable throwable) throws Exception {
                Log.d(TAG, "accept() called with: throwable = [" + throwable + "]");
            }
        };
    }

    private Action provideOnCompleteConsumer() {
        return new Action() {
            @Override
            public void run() throws Exception {
                Log.d(TAG, "run() called");
            }
        };
    }


    public void clearData(){
        movieDAO.deleteAll();
    }
}
