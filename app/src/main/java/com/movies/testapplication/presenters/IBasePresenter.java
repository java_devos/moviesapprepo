package com.movies.testapplication.presenters;

import java.util.List;


public interface IBasePresenter<T, N>{

    void obtainEntities();

    void persistEntities(List<T> tList);

    void stopInteraction();

}
