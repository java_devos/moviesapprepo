package com.movies.testapplication.constants;


import android.content.UriMatcher;
import android.net.Uri;
import android.provider.BaseColumns;

import java.util.HashMap;

public class Constants {

    public static class Database {

        @SuppressWarnings("WeakerAccess")
        public static class DataProviders {

            public static final String DATABASE = "stestdb";

            public static final int DB_VERSION = 2;

            public static final String TABLE_NAME = "movies";

            public static final String AUTHORITY = "com.movies.testapplication.AppContentProvider";

            public static final String SCHEME = "content://";

            public static final String MOVIES_PATH = "movies";

            public static final String CONTENT_URL = SCHEME + AUTHORITY;

            public static final Uri CONTENT_URI = Uri.parse(SCHEME + AUTHORITY);

            public static final int FLAG_MOVIES = 1;

            public static final int FLAG_STEPS_OBTAIN = 2;

            public static final UriMatcher uriMatcher;

            public static HashMap<String, String> CONSTANTS_MOVIES_LIST_PROJECTION;

            static {

                uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
                uriMatcher.addURI(AUTHORITY, MOVIES_PATH + "/", FLAG_MOVIES);


                CONSTANTS_MOVIES_LIST_PROJECTION = new HashMap<>();
                CONSTANTS_MOVIES_LIST_PROJECTION.put(MoviesProviderData.COLUMN_ID, MoviesProviderData.COLUMN_ID);
                CONSTANTS_MOVIES_LIST_PROJECTION.put(MoviesProviderData.COLUMN_TITLE, MoviesProviderData.COLUMN_TITLE);
                CONSTANTS_MOVIES_LIST_PROJECTION.put(MoviesProviderData.COLUMN_RATING, MoviesProviderData.COLUMN_RATING);
                CONSTANTS_MOVIES_LIST_PROJECTION.put(MoviesProviderData.COLUMN_YEAR, MoviesProviderData.COLUMN_YEAR);
                CONSTANTS_MOVIES_LIST_PROJECTION.put(MoviesProviderData.COLUMN_IMAGE, MoviesProviderData.COLUMN_IMAGE);
                CONSTANTS_MOVIES_LIST_PROJECTION.put(MoviesProviderData.COLUMN_GENRE, MoviesProviderData.COLUMN_GENRE);

            }

        }

        public static class Query{

            public static final String MOVIES_DATABASE_CREATE =
                    "create table " + DataProviders.TABLE_NAME + "(" +
                            MoviesProviderData.COLUMN_ID + " integer primary key autoincrement, " +
                            MoviesProviderData.COLUMN_TITLE + " text, " +
                            MoviesProviderData.COLUMN_YEAR + " integer, " +
                            MoviesProviderData.COLUMN_RATING + " text, " +
                            MoviesProviderData.COLUMN_GENRE + " text, " +
                            MoviesProviderData.COLUMN_IMAGE + " text" +
                            ");";

            public static  final String MOVIES_TABLE_DROP = "DROP TABLE IF EXISTS " + DataProviders.TABLE_NAME;

        }

        @SuppressWarnings("WeakerAccess")
        public static class MoviesProviderData implements BaseColumns {

            public static final String COLUMN_ID = "id";

            public static final String COLUMN_TITLE = "title";

            public static final String COLUMN_YEAR = "year";

            public static final String COLUMN_RATING = "rating";

            public static final String COLUMN_GENRE = "genre";

            public static final String COLUMN_IMAGE = "image";

        }

    }

    public static class URN {

        public static final String MOVIES_URN = "json/movies.json";

    }
}
