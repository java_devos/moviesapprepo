package com.movies.testapplication.di.modules;

import com.movies.testapplication.api.clients.rest.RetrofitClient;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class NetworkModule {

    @Provides
    @Singleton
    RetrofitClient getRetrofitClient() {
        return new RetrofitClient();
    }
}
