package com.movies.testapplication.di;

import com.movies.testapplication.di.modules.ApiClientsModule;
import com.movies.testapplication.di.modules.ApplicationModule;
import com.movies.testapplication.di.modules.DatabaseModule;
import com.movies.testapplication.di.modules.NetworkModule;
import com.movies.testapplication.presenters.movies.MoviesActivityPresenter;
import com.movies.testapplication.ui.activity.main.MainActivity;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {ApplicationModule.class, NetworkModule.class, DatabaseModule.class, ApiClientsModule.class})
public interface MainComponent {

    void inject(MoviesActivityPresenter moviesActivityPresenter);

    void inject(MainActivity mainActivity);

}
