package com.movies.testapplication.di.modules;

import android.content.Context;

import com.movies.testapplication.database.dao.MovieDAO;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class DatabaseModule {

    private Context context;

    public DatabaseModule(Context context) {
        this.context = context;
    }

    @Provides
    @Singleton
    MovieDAO getMovieDAO() {
        return new MovieDAO(context);
    }
}
