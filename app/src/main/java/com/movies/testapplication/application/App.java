package com.movies.testapplication.application;

import android.app.Application;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;

import com.movies.testapplication.di.DaggerMainComponent;
import com.movies.testapplication.di.MainComponent;
import com.movies.testapplication.di.modules.ApplicationModule;
import com.movies.testapplication.di.modules.DatabaseModule;
import com.movies.testapplication.di.modules.NetworkModule;

public class App extends Application {

    private static MainComponent mainComponent;

    public static MainComponent getMainComponent() {
        return mainComponent;
    }

    private PackageManager packManager;

    @Override
    public void onCreate() {
        super.onCreate();

        packManager = this.getPackageManager();

        mainComponent = DaggerMainComponent
                .builder()
                .applicationModule(new ApplicationModule(this))
                .networkModule(new NetworkModule())
                .databaseModule(new DatabaseModule(this))
                .build();
    }

    public String getAppVersion() {

        PackageInfo info = null;
        try {
            info = packManager.getPackageInfo(
                    this.getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        return info != null ? info.versionName : "0";
    }
}
