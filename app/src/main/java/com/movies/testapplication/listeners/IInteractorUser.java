package com.movies.testapplication.listeners;

import com.movies.testapplication.model.movie.Movie;

import java.util.List;



public interface IInteractorUser<T, N, E> {

    T entitiesObtaining(N n, E e);

    T entitiesProviding(N n, E e);

    void stopInteraction();

    void clearData();

}
