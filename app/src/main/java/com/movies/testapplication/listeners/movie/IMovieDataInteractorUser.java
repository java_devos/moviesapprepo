package com.movies.testapplication.listeners.movie;

import com.movies.testapplication.listeners.IInteractorUser;
import com.movies.testapplication.model.movie.Movie;
import com.movies.testapplication.ui.adapter.MovieRVAdapter;

import java.util.List;


public interface IMovieDataInteractorUser extends IInteractorUser<List<Movie>, List<Movie>, Void> {

    @Override
    List<Movie> entitiesObtaining(List<Movie> movies, Void aVoid);
}
