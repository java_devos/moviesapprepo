package com.movies.testapplication.listeners.movie;

import com.movies.testapplication.model.movie.Movie;
import com.movies.testapplication.listeners.IInteractorUser;

import java.util.List;


public interface IMovieDataProviderInteractor extends IInteractorUser<List<Movie>, List<Movie>, IMovieDataInteractorUser> {

    List<Movie> entitiesProviding(List<Movie> movieList, IMovieDataInteractorUser interactor);

}
