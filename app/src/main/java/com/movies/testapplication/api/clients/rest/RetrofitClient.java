package com.movies.testapplication.api.clients.rest;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.movies.testapplication.BuildConfig;
import com.movies.testapplication.api.gateways.rest.RestApi;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClient {

    private RestApi client;

    public RetrofitClient() {
        this.client = buildRestClient();
    }

    public RestApi getClientApi() {
        return client;
    }

    private RestApi buildRestClient(){
        return setupClient(BuildConfig.restApiBaseUrl).create(RestApi.class);
    }

    private Retrofit setupClient(String baseUrl) {

        return new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create(getParser()))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(getSubClient())
                .build();
    }

    private OkHttpClient getSubClient() {
        return new OkHttpClient.Builder()
                .build();
    }

    private Gson getParser() {
        return new GsonBuilder()
                .create();
    }

}
