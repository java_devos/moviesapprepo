package com.movies.testapplication.api.gateways.rest;

import com.movies.testapplication.constants.Constants;
import com.movies.testapplication.model.movie.Movie;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.GET;


public interface RestApi{

    @GET(Constants.URN.MOVIES_URN)
    Observable<List<Movie>> getPhones();

}
