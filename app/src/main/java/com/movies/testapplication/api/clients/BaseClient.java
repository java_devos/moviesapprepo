package com.movies.testapplication.api.clients;

import android.content.Context;


public abstract class BaseClient implements IBaseClient {

    private static final String TAG = BaseClient.class.getSimpleName();

    private Context context;

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }
}
