package com.movies.testapplication.api.clients;

import com.movies.testapplication.api.gateways.API;


public interface IBaseClient {

    API getClientApi();

}
