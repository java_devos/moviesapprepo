package com.movies.testapplication.database;

import java.util.List;

public interface IDataSource<T, N> {

    List<T> obtainAll();

    T getByIdentifier(N identifier);

    void deleteByIdentifier(N identifier);

    void save(T object);

    void update(T object);

    void deleteAll();

}
