package com.movies.testapplication.database.provider;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.movies.testapplication.constants.Constants;
import com.movies.testapplication.database.tools.AppDatabaseErrorHandler;
import com.movies.testapplication.database.tools.AppDataSourceHelper;

import java.util.HashMap;

public class AppContentProvider extends ContentProvider {

    @SuppressWarnings("FieldCanBeLocal")
    private AppDataSourceHelper dataSourceHelper;

    @SuppressWarnings("FieldCanBeLocal")
    private AppDatabaseErrorHandler databaseErrorHandler;

    private SQLiteDatabase database;


    @Override
    public boolean onCreate() {

        databaseErrorHandler = new AppDatabaseErrorHandler();
        dataSourceHelper = new AppDataSourceHelper(getContext(), databaseErrorHandler);
        database = dataSourceHelper.getWritableDatabase();

        return (database != null);
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public Cursor query(@NonNull Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {

        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();


        switch (Constants.Database.DataProviders.uriMatcher.match(uri)) {

            case Constants.Database.DataProviders.FLAG_MOVIES:
                queryBuilder.setTables(Constants.Database.DataProviders.TABLE_NAME);
                queryBuilder.setProjectionMap(getMoviesTableProjection());
                break;

            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }

        Cursor cursor = queryBuilder.query(database, projection, selection, selectionArgs, null, null, null);
        cursor.setNotificationUri(getContext().getContentResolver(), uri);

        return cursor;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        switch (Constants.Database.DataProviders.uriMatcher.match(uri)) {

            case Constants.Database.DataProviders.FLAG_MOVIES:
                return "vnd.android.cursor.dir/vnd.movies.testapplication";

            default:
                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public Uri insert(@NonNull Uri uri, ContentValues values) {

        long row;

        switch (Constants.Database.DataProviders.uriMatcher.match(uri)) {

            case Constants.Database.DataProviders.FLAG_MOVIES:
                row = database.insert(Constants.Database.DataProviders.TABLE_NAME, "", values);
                break;
            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }

        if (row > 0) {

            Uri newUri = ContentUris.withAppendedId(Constants.Database.DataProviders.CONTENT_URI, row);
            getContext().getContentResolver().notifyChange(newUri, null);
            return newUri;
        }

        throw new SQLException("Fail to add a new record into " + uri);
    }

    @Override
    public int delete(@NonNull Uri uri, String selection, String[] selectionArgs) {

        switch (Constants.Database.DataProviders.uriMatcher.match(uri)) {

            case Constants.Database.DataProviders.FLAG_MOVIES:
                clearTable();
                return 0;
            default:
                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }

    }

    @Override
    public int update(@NonNull Uri uri, ContentValues values, String selection, String[] selectionArgs) {

        return 0;

    }

    private HashMap<String, String> getMoviesTableProjection() {
        return (Constants.Database.DataProviders.CONSTANTS_MOVIES_LIST_PROJECTION);
    }

    public void clearTable() {
        database.delete(Constants.Database.DataProviders.TABLE_NAME, null, null);
    }
}
