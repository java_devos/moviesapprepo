package com.movies.testapplication.database;

import android.content.Context;
import android.database.Cursor;

import com.movies.testapplication.database.tools.AppDataSourceHelper;

public abstract class BaseDataSource<T, N> implements IDataSource<T, N> {

    protected Context context;

    protected AppDataSourceHelper appDataSourceHelper;

    protected BaseDataSource(Context context) {
        this.context = context;
    }

    protected void closeDatabase() {
        if (appDataSourceHelper != null) {
            appDataSourceHelper.close();
        }
    }

    protected void closeCursor(Cursor cursor) {
        if (cursor != null) {
            cursor.close();
        }
    }
}
