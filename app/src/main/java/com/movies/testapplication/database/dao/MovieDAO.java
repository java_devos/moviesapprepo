package com.movies.testapplication.database.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

import com.movies.testapplication.constants.Constants;
import com.movies.testapplication.database.BaseDataSource;
import com.movies.testapplication.model.movie.Movie;

import java.util.ArrayList;
import java.util.List;


public class MovieDAO extends BaseDataSource<Movie, Long> {

    public MovieDAO(Context context) {
        super(context);
    }

    @Override
    public List<Movie> obtainAll() {

        Uri obtainAllUri = Uri.parse(Constants.Database.DataProviders.CONTENT_URL + "/" + Constants.Database.DataProviders.MOVIES_PATH + "/");
        Cursor cursor = context.getContentResolver().query(obtainAllUri, null, null, null, null);

        ArrayList<Movie> movies = new ArrayList<>();
        try {

            if (cursor.getCount() > 0) {

                cursor.moveToFirst();

                while (!cursor.isAfterLast()) {

                    Movie movie = new Movie();

                    movie.setId(cursor.getInt(cursor.getColumnIndex(Constants.Database.MoviesProviderData.COLUMN_ID)));
                    movie.setTitle(cursor.getString(cursor.getColumnIndex(Constants.Database.MoviesProviderData.COLUMN_TITLE)));
                    movie.setReleaseYear(cursor.getInt(cursor.getColumnIndex(Constants.Database.MoviesProviderData.COLUMN_YEAR)));
                    movie.setRating(Float.valueOf(cursor.getString(cursor.getColumnIndex(Constants.Database.MoviesProviderData.COLUMN_RATING))));
                    movie.setParsedGenres(cursor.getString(cursor.getColumnIndex(Constants.Database.MoviesProviderData.COLUMN_GENRE)));
                    movie.setImage(cursor.getString(cursor.getColumnIndex(Constants.Database.MoviesProviderData.COLUMN_IMAGE)));

                    movies.add(movie);

                    cursor.moveToNext();
                }
            }
        } finally {

            closeCursor(cursor);
        }

        return movies;
    }

    @Override
    public Movie getByIdentifier(Long identifier) {
        return null;
    }

    @Override
    public void deleteByIdentifier(Long identifier) {

    }

    @Override
    public void save(Movie object) {

        ContentValues saveContentValues = new ContentValues();

        saveContentValues.put(Constants.Database.MoviesProviderData.COLUMN_TITLE, object.getTitle());
        saveContentValues.put(Constants.Database.MoviesProviderData.COLUMN_YEAR, object.getReleaseYear());
        saveContentValues.put(Constants.Database.MoviesProviderData.COLUMN_RATING, object.getRating());
        saveContentValues.put(Constants.Database.MoviesProviderData.COLUMN_GENRE, object.getParsedGenres());
        saveContentValues.put(Constants.Database.MoviesProviderData.COLUMN_IMAGE, object.getImage());

        Uri saveUri = Uri.parse(Constants.Database.DataProviders.CONTENT_URL + "/" + Constants.Database.DataProviders.MOVIES_PATH + "/");
        context.getContentResolver().insert(saveUri, saveContentValues);
    }

    @Override
    public void update(Movie object) {

    }

    @Override
    public void deleteAll() {
        Uri deleteUri = Uri.parse(Constants.Database.DataProviders.CONTENT_URL + "/" + Constants.Database.DataProviders.MOVIES_PATH + "/");
        context.getContentResolver().delete(deleteUri, null, null);
    }
}
